terraform {
  backend "s3" {
    bucket = "academyserverlessexample" # ! REPLACE WITH YOUR TERRAFORM BACKEND BUCKET
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }
}
variable "S3_BACKEND_BUCKET" {
  default = "academyserverlessexample" # ! REPLACE WITH YOUR TERRAFORM BACKEND BUCKET
}

variable "S3_BUCKET_REGION" {
  type    = "string"
  default = "eu-west-2"
}

variable "AWS_REGION" {
  type    = "string"
  default = "eu-west-2"
}

variable "TAG_ENV" {
  default = "dev"
}

variable "ENV" {
  default = "PROD"
}

variable "s3_bucket" {
  default = "academyserverlessexample"
}

variable "s3_key" {
  default = "/example.zip"
}

variable "version" {
  default = "v1.0.2"
}

variable "azure_resource_group" {
  default = "AL_Academy"
}

variable "azure_location" {
  default = "westeurope"
}
