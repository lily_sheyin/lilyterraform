provider "azurerm" { }

resource "azurerm_mysql_server" "db" {
  name                = "lilydb"
  location            = "${var.azure_location}"
  resource_group_name = "${var.azure_resource_group}"

  sku {
    name = "MYSQLB50"
    capacity = 50
    tier = "Basic"
  }

  administrator_login = "lily"
  administrator_login_password = "ALsecret1234"
  version = "5.7"
  storage_mb = "51200"
  ssl_enforcement = "Disabled"
}

resource "azurerm_mysql_database" "initialdb" {
  name                = "petclinic"
  resource_group_name = "${var.azure_resource_group}"
  server_name         = "${azurerm_mysql_server.db.name}"
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

resource "azurerm_mysql_firewall_rule" "firewall" {
  name                = "lilypc1"
  resource_group_name = "${var.azure_resource_group}"
  server_name         = "${azurerm_mysql_server.db.name}"
  start_ip_address    = "${aws_instance.petclinic1.public_ip}"
  end_ip_address      = "${aws_instance.petclinic1.public_ip}"
}


resource "azurerm_mysql_firewall_rule" "firewall2" {
  name                = "lilypc2"
  resource_group_name = "${var.azure_resource_group}"
  server_name         = "${azurerm_mysql_server.db.name}"
  start_ip_address    = "${aws_instance.petclinic2.public_ip}"
  end_ip_address      = "${aws_instance.petclinic2.public_ip}"
}

resource "azurerm_mysql_firewall_rule" "firewall3" {
  name                = "aloffice"
  resource_group_name = "${var.azure_resource_group}"
  server_name         = "${azurerm_mysql_server.db.name}"
  start_ip_address    = "77.108.144.180"
  end_ip_address      = "77.108.144.180"
}
