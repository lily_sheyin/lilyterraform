resource "aws_instance" "petclinic1" {
  ami           = "${var.instance_ami}"
  instance_type = "t2.micro"
  key_name = "${var.aws_key_name}"
  subnet_id = "${module.base_vpc.public_subnets[0]}"
  vpc_security_group_ids = ["${aws_security_group.lilypc.id}"]
  associate_public_ip_address = true
  iam_instance_profile = "s3Vault"
  user_data = "${file("userdata/petclinic.sh")}"

  tags {
    Name = "lilypc1"
  }
}

resource "aws_instance" "petclinic2" {
  ami           = "${var.instance_ami}"
  instance_type = "t2.micro"
  key_name = "${var.aws_key_name}"
  subnet_id = "${module.base_vpc.public_subnets[0]}"
  vpc_security_group_ids = ["${aws_security_group.lilypc.id}"]
  associate_public_ip_address = true
  iam_instance_profile = "s3Vault"
  user_data = "${file("userdata/petclinic.sh")}"

  tags {
    Name = "lilypc2"
  }
}
