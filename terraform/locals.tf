# Workaround, allowing interpolation in variables
locals {
  type="list"
  azs = ["${var.AWS_REGION}a","${var.AWS_REGION}b"]
}
