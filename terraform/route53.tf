resource "aws_route53_record" "petclinic1" {
  zone_id = "ZJZM4EGXLRUOD"
  name    = "lily.lzs.grads.al-labs.co.uk"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.petclinic1.public_ip}"]
}

resource "aws_route53_record" "petclinic2" {
  zone_id = "ZJZM4EGXLRUOD"
  name    = "lily.lzs.grads.al-labs.co.uk"
  type    = "A"
  ttl     = "300"
  records = ["${aws_instance.petclinic2.public_ip}"]
}
