#!/bin/bash

(sleep 5; echo yes) | terraform apply --target aws_ecr_repository.lzspetclinic

export REPO_URL=$(terraform output lzspetclinic-repo)

$(aws ecr get-login --region us-east-1 --no-include-email)

cd ../docker/lzspetclinic/

./upload.sh
