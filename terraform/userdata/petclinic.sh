#!/bin/bash
yum -y install java-1.8.0-openjdk-1.8.0.151-1.b12.35.amzn1.x86_64 java-1.8.0-openjdk-headless-1.8.0.151-1.b12.35.amzn1.x86_64 java-1.8.0-openjdk-devel-1.8.0.151-1.b12.35.amzn1.x86_64
yum -y erase java-1.7.0-openjdk

mkdir /opt/target

cat > /opt/target/application.properties <<_END_
database=mysql
spring.datasource.url=jdbc:mysql://lilydb.mysql.database.azure.com:3306/petclinic
spring.datasource.username=lily@lilydb
spring.datasource.password=ALsecret1234
spring.datasource.initialize=true
spring.datasource.schema=classpath*:db/${database}/schema.sql
spring.datasource.data=classpath*:db/${database}/data.sql

# Web
spring.thymeleaf.mode=HTML

# JPA
spring.jpa.hibernate.ddl-auto=none

# Internationalization
spring.messages.basename=messages/messages

# Actuator / Management
management.endpoints.web.base-path=/manage
management.endpoints.web.exposure.include=*

# Logging
logging.level.org.springframework=INFO
# logging.level.org.springframework.web=DEBUG
# logging.level.org.springframework.context.annotation=TRACE

# Active Spring profiles
spring.profiles.active=production
_END_

aws s3 cp s3://academybuckettest/spring-petclinic-2.0.0.jar /opt/target/spring-petclinic-2.0.0.jar

cat >/etc/init.d/petclinic <<'_END_'
#!/bin/bash

#description: Petclinic control script
#chkconfig: 2345 99 99

case $1 in
  'start')
    # The next 2 lines are for running PC from a pre-compiled jar
    cd /opt/target
    java -jar /opt/target/spring-petclinic-2.0.0.jar >/var/log/petclinic.stdout 2>/var/log/petclinic.stderr &
    ;;
  'stop')
    kill $(ps -ef | grep petclinic | grep -v grep | awk '{print $2}')
    ;;
  'status')
    PID=$(ps -ef | grep java | grep petclinic | grep -v grep | awk '{print $2}')
    if [[ -n $PID ]]
    then
      echo "Petclinic is running with PID $PID"
    fi
    ;;
  *)
    echo "I do not understand that option"
    ;;
esac
_END_

chmod +x /etc/init.d/petclinic

chkconfig --add petclinic
service petclinic start
