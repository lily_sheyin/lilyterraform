terraform {
  backend "s3" {
    bucket = "lily-terraform" # ! REPLACE WITH YOUR TERRAFORM BACKEND BUCKET
    key    = "terraform.tfstate"
    region = "eu-west-2"
  }
}
variable "S3_BACKEND_BUCKET" {
  default = "lily-terraform" # ! REPLACE WITH YOUR TERRAFORM BACKEND BUCKET
}

variable "S3_BUCKET_REGION" {
  type    = "string"
  default = "eu-west-2"
}

variable "AWS_REGION" {
  type    = "string"
  default = "eu-west-2"
}

variable "TAG_ENV" {
  default = "dev"
}

variable "ENV" {
  default = "PROD"
}

variable "vpc_cidr" {
  default = "10.2.0.0/16"
}

variable "CIDR_PUBLIC" {
  default = "10.2.2.0/24"
}

variable "aws_key_name" {
  default = "lilyaws"
}

variable "instance_ami" {
  default = "ami-403e2524"
}

variable "azure_resource_group" {
  default = "AL_Academy"
}

variable "azure_location" {
  default = "westeurope"
}
