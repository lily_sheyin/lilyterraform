
module "base_vpc" {
  source = "github.com/terraform-aws-modules/terraform-aws-vpc"

  name = "lily_vpc"
  cidr = "${var.vpc_cidr}"

  azs                 = ["${local.azs}"]
  public_subnets      = ["${var.CIDR_PUBLIC}"]

  enable_nat_gateway = false

  tags = {
    Terraform = "true"
    Environment = "dev"
  }
}
